import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { AuthorsComponent } from './authors/authors.component';
import { AuthorService } from './services/author.service';

@NgModule({
  declarations: [
  	AuthorsComponent,
    AppComponent
  ],
  imports: [
    BrowserModule
  ],
  providers: [AuthorService],
  bootstrap: [AppComponent]
})
export class AppModule { }

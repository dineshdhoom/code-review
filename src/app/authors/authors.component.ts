import { Component, OnInit } from '@angular/core';
import { AuthorService } from '../services/author.service';


@Component({
  selector: 'authors',
  templateUrl: './authors.component.html',
  styleUrls: ['./authors.component.css']
})
export class AuthorsComponent implements OnInit {
  authors: any = [];

  constructor(private authService: AuthorService,) { }

  ngOnInit() {
  	this.authors = this.authService.getAllAuthors();
  	return this;
  }

}

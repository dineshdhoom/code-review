import { Injectable } from '@angular/core';

@Injectable()
export class AuthorService {
    constructor() { }

    getAllAuthors() {
        return ['Author1', 'Author2', 'Author3', 'Author4'];
    }
}